#include <math.h>
#include <getopt.h>
#include <signal.h>
#include <sys/file.h>
#include <linux/input.h>
#include <gtkmm.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>
#include <X11/Xatom.h>

#define EVENT_TYPE      EV_ABS
#define EVENT_CODE_X    ABS_X
#define EVENT_CODE_Y    ABS_Y


typedef struct {
    double X;
    double Y;
} point_t;

typedef struct {
    int32_t xmin, xmax, ymin, ymax;
    bool invert_x, invert_y, swap_xy;
} calib_out_t;

typedef enum {
    STATE_IDDLE,
    STATE_WAITING,
    STATE_CHECKED,
    STATE_STOPPED
} point_state_t;

typedef struct {
    point_state_t state;
    unsigned int cyclenum;
} point_info_t;

class CalibrationArea;

class Worker {
public:
    void start ();
    Worker(char *devname);
    ~Worker();
    sigc::signal<bool, int, int> sig_press;
protected:
    void run ();
    uint32_t X, Y;
    Glib::Thread * thread;
    Glib::Mutex mutex;
    char *devicename;
    bool do_stop;
};

static Display *display;
static bool rotate_points = false;

class CalibrationArea : public Gtk::DrawingArea {
public:
    CalibrationArea(Gtk::Main *papp, char *devicename, int deviceid, bool m_autorotate);
    ~CalibrationArea();

    void set_display_size(int width, int height);

protected:
    point_t draws[4];
    point_t points[4];
    int device_id;
    bool autorotate;
    point_info_t infos[4];
    Worker *evdev_worker;
    int current_point_num;
    Gtk::Main *app;
    double start_time;
    int display_width, display_height;
    int time_elapsed;
    Glib::RefPtr<Gdk::PixbufAnimation> image;
    Glib::RefPtr<Gdk::PixbufAnimationIter> iter;
    bool on_timer_signal();
    bool on_evdev_signal(int X, int Y);
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
    bool on_button_press_event(GdkEventButton *event);
    bool on_key_press_event(GdkEventKey *event);
    void calib_calc(calib_out_t &out);
    void redraw();
    void draw_points();
    void apply_axys(calib_out_t &outdata);
};


const int time_step = 15;  // in milliseconds
const int num_blocks = 8;
const int time_seconds = 30;

CalibrationArea::CalibrationArea(Gtk::Main *papp, char *devicename, int deviceid, bool m_autorotate)
 : app(papp), autorotate(m_autorotate)
{
    add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK | Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK);
    set_can_focus(true);
    set_display_size(get_width(), get_height());
    sigc::slot<bool> slot = sigc::mem_fun(*this, &CalibrationArea::on_timer_signal);

    image = Gdk::PixbufAnimation::create_from_file("/usr/share/evdevcalib/huge.gif");
    Glib::TimeVal now;
    now.assign_current_time();
    start_time = now.as_double();

    iter = image->get_iter(&now);

    for (int i = 0; i < 4; i++) {
	infos[i].state = STATE_IDDLE;
	infos[i].cyclenum = 0;
    }
    current_point_num = 0;
    infos[current_point_num].state = STATE_WAITING;
    evdev_worker = new Worker(devicename);
    device_id = deviceid;
    evdev_worker->sig_press.connect(sigc::mem_fun(*this, &CalibrationArea::on_evdev_signal));
    Glib::signal_timeout().connect(slot, time_step);
    evdev_worker->start();
}

void CalibrationArea::apply_axys(calib_out_t &outdata) {
    Atom prop_calibration = XInternAtom(display, "Evdev Axis Calibration", False);

    if (!prop_calibration) {
        fprintf(stderr, "Evdev axis calibration not found. This "
                "server is too old\n");
    } else {
          XIChangeProperty(display, device_id, prop_calibration, XA_INTEGER,
                     sizeof(uint32_t)*8, PropModeReplace, reinterpret_cast<unsigned char*>(&outdata), 4);
          XSync(display, 0);
    }
}

CalibrationArea::~CalibrationArea() {
    delete evdev_worker;
}

Worker::Worker(char *devname) : thread(0), do_stop(false), devicename(devname) {
    X = 0;
    Y = 0;
}

Worker::~Worker() {
    {
       Glib::Mutex::Lock lock (mutex);
       do_stop = true;
    }
    if (thread)
        thread->join();
}

void Worker::start () {
    thread = Glib::Thread::create(sigc::mem_fun(*this, &Worker::run), true);
}

void Worker::run() {
    struct input_event ev;

    int fd = open(devicename, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr, "%s is not a vaild device\n", devicename);
    } else {
        Glib::TimeVal now;
        now.assign_current_time();
        double last_time = now.as_double();
        uint8_t cnt = 0;

        ioctl(fd, EVIOCGRAB, (void*)1);
        fprintf(stderr, "Listen data from %s...\n", devicename);

        const int flags = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, flags | O_NONBLOCK);
        const size_t ev_size = sizeof(struct input_event);

        while (true) {
            {
               Glib::Mutex::Lock lock (mutex);
               if (do_stop) break;
            }
            ssize_t size = read(fd, &ev, ev_size);
            if (size != (-1) && size >= ev_size) {
               now.assign_current_time();

               for (int i = 0; i < size / ev_size; i++) {
                  if (ev.type == EVENT_TYPE &&
                      (ev.code == EVENT_CODE_X || ev.code == EVENT_CODE_Y)) {
                      if (ev.code == EVENT_CODE_X) {
                         if ((X != ev.value) && (ev.value != 0)) {
                             X = ev.value;
                             cnt |= 1;
                             last_time = now.as_double();
                         }
                      }
                      else {
                         if ((Y != ev.value) && (ev.value != 0)) {
                             Y = ev.value;
                             cnt |= 2;
                             last_time = now.as_double();
                         }
                      }
                   }
               }
           }
           if (((cnt & 3) == 1 || (cnt & 3) == 2) && now.as_double() - last_time > 0.01) {
              cnt = 0;
           } else if ((cnt & 3) == 3)  {
              sig_press(X, Y);
              cnt = 0;
           }
           usleep(50);
        }
        ioctl(fd, EVIOCGRAB, (void*)0);
    }
}

bool CalibrationArea::on_evdev_signal(int X, int Y) {
    Glib::TimeVal now;
    now.assign_current_time();
    if (current_point_num > 0) {
	int old_X = (int)points[current_point_num-1].X;
	int old_Y = (int)points[current_point_num-1].Y;
	if (abs(X - old_X) <= 100 && abs(Y - old_Y) <= 100)
	    return false;
    } else if (X == 0 || Y == 0)
	return false;
    start_time = now.as_double();
    points[current_point_num].X = (double)X;
    points[current_point_num].Y = (double)Y;

    infos[current_point_num].cyclenum = 0;
    infos[current_point_num].state = STATE_CHECKED;
    if (current_point_num < 3) {
	current_point_num++;
	infos[current_point_num].cyclenum = 0;
	infos[current_point_num].state = STATE_WAITING;
    } else {
	calib_out_t out;
	calib_calc(out);
	apply_axys(out);
	fprintf(stdout, "%d:%d:%d:%d:%s:%s:%s\n", out.xmin, out.xmax, out.ymin, out.ymax, out.swap_xy ? "true" : "false", out.invert_x ? "true" : "false", out.invert_y ? "true" : "false");
	app->quit();
    }
    return true;
}

void CalibrationArea::draw_points() {
    Glib::RefPtr<Gdk::Window> window = get_window();
    if (window) {
        Cairo::RefPtr<Cairo::Context> cr = window->create_cairo_context();
        cr->save();

	const int delta_x = display_width/num_blocks;
	const int delta_y = display_height/num_blocks;

	Glib::TimeVal now;
	now.assign_current_time();
	iter->advance(now);
	Glib::RefPtr<Gdk::Pixbuf> pixbuf = iter->get_pixbuf();
	double max_height = display_height / 3;
	double width  = pixbuf->get_width();
	double height = pixbuf->get_height();

	Glib::RefPtr<Gdk::Pixbuf> imageS;
	double scale = 1;
	if (height > max_height && max_height > 0) {
	    scale = max_height / height;
	    width = width * scale;
	    height = max_height;
	    imageS = pixbuf->scale_simple(width, height, Gdk::INTERP_BILINEAR);
	} else imageS = pixbuf;
	double cross_size = 20 * scale;

	double imgx = (display_width - width) / 2;
	double imgy = (display_height - height) / 2;

	Gdk::Cairo::set_source_pixbuf(cr, imageS, imgx, imgy );
	cr->rectangle(imgx, imgy, width, height);
	cr->fill();

	double delta = (now.as_double() - start_time);
	if (delta > time_seconds) {
	    delta = 0;
	    start_time = now.as_double();
	}
	char strtime[32];
	sprintf(strtime, "%0.2f", time_seconds - delta);

	cr->set_font_size(32 * scale);
	cr->select_font_face ("Bitstream Vera Sans",
	        Cairo::FontSlant::FONT_SLANT_NORMAL,
	        Cairo::FontWeight::FONT_WEIGHT_BOLD);
	Cairo::TextExtents extent;

        cr->set_source_rgb(1.0, 1.0, 1.0);
	cr->get_text_extents(strtime, extent);
	double text_width = extent.width;
	double text_height = extent.height + 2;

	double x = (display_width - text_width) / 2;
	double y = imgy - (text_height);

	cr->get_text_extents("959595", extent);
	cr->rectangle((display_width - extent.width) / 2, imgy - (extent.height * 2.5), extent.width, extent.height * 1.5);
	cr->fill();

        cr->set_source_rgb(0.5, 0.5, 0.5);
	cr->move_to(x, y);
	cr->show_text(strtime);
	cr->stroke();

	for (int i = 0; i < 4; i++) {
	    if (infos[i].state == STATE_WAITING) {
		cr->set_source_rgb(1.0, 1.0, 1.0);
		cr->rectangle(draws[i].X-cross_size, draws[i].Y-cross_size, cross_size*2, cross_size*2);
		cr->fill();

		cr->set_source_rgb(0.0, 0.0, 0.0);
		cr->set_line_width(2);
		double grad  = infos[i].cyclenum * 2 * M_PI / 180;
		double grad2 = ((infos[i].cyclenum * 2) + 90) * M_PI / 180;
		cr->move_to(draws[i].X + cross_size * cos(grad), draws[i].Y + cross_size * sin(grad));
		cr->line_to(draws[i].X - cross_size * cos(grad), draws[i].Y - cross_size * sin(grad));
		cr->move_to(draws[i].X + cross_size * cos(grad2), draws[i].Y + cross_size * sin(grad2));
		cr->line_to(draws[i].X - cross_size * cos(grad2), draws[i].Y - cross_size * sin(grad2));
		cr->stroke();
		infos[i].cyclenum++;
		if (infos[i].cyclenum > 180)
		    infos[i].cyclenum = 0;
	    } else if (infos[i].state == STATE_CHECKED) {
		if (infos[i].cyclenum < cross_size) {
		    cr->set_source_rgb(1.0, 1.0, 1.0);
		    cr->rectangle(draws[i].X-cross_size, draws[i].Y-cross_size, cross_size*2, cross_size*2);
		    cr->fill();
		}
		cr->set_source_rgb(1.0, 0.02 * infos[i].cyclenum, 0.02 * infos[i].cyclenum);
		cr->arc(draws[i].X, draws[i].Y, infos[i].cyclenum * 2 * scale, 0, 2 * M_PI);
		cr->fill();

		double grad  = infos[i].cyclenum * 2 * M_PI / 180;
		double grad2 = ((infos[i].cyclenum * 2) + 90) * M_PI / 180;
		cr->set_source_rgb(0.0, 0.0, 0.0);
		cr->set_line_width(2);
		cr->move_to(draws[i].X + cross_size * cos(grad), draws[i].Y + cross_size * sin(grad));
		cr->line_to(draws[i].X - cross_size * cos(grad), draws[i].Y - cross_size * sin(grad));
		cr->move_to(draws[i].X + cross_size * cos(grad2), draws[i].Y + cross_size * sin(grad2));
		cr->line_to(draws[i].X - cross_size * cos(grad2), draws[i].Y - cross_size * sin(grad2));
		cr->stroke();

		infos[i].cyclenum++;
		if (infos[i].cyclenum > 60) {
		    infos[i].cyclenum = 0;
		    infos[i].state = STATE_STOPPED;
		}
	    } else if (infos[i].state == STATE_STOPPED) {
		if (infos[i].cyclenum == 0) {
		    cr->set_source_rgb(1.0, 1.0, 1.0);
		    cr->rectangle(draws[i].X-60, draws[i].Y-60, 60*2, 60*2);
		    cr->fill();

		    cr->set_source_rgb(0.6, 0.6, 0.6);
		    cr->set_line_width(6);
		    cr->move_to(draws[i].X + 10, draws[i].Y);
		    cr->line_to(draws[i].X - 10, draws[i].Y);
		    cr->move_to(draws[i].X, draws[i].Y + 10);
		    cr->line_to(draws[i].X, draws[i].Y - 10);
		    cr->stroke();

		    cr->set_source_rgb(1.0, 0.3, 0.3);

		    cr->move_to(draws[i].X + 12, draws[i].Y + 2);
		    cr->line_to(draws[i].X - 8, draws[i].Y + 2);
		    cr->move_to(draws[i].X + 2, draws[i].Y + 12);
		    cr->line_to(draws[i].X + 2, draws[i].Y - 8);

		    cr->stroke();
		    infos[i].cyclenum++;
		}
		if (current_point_num < i)
		    infos[i].state = STATE_IDDLE;
	    }
	}
	cr->restore();
    }
}

void CalibrationArea::set_display_size(int width, int height) {
    display_width = width;
    display_height = height;

    const int delta_x = display_width/num_blocks;
    const int delta_y = display_height/num_blocks;

    draws[0] = { 1.0f * delta_x, 1.0f * delta_y };
    draws[1] = { 1.0f * (display_width - delta_x - 1), 1.0f * delta_y };
    draws[2] = { 1.0f * delta_x, 1.0f * (display_height - delta_y - 1) };
    draws[3] = { 1.0f * (display_width - delta_x - 1), 1.0f * (display_height - delta_y - 1) };
}

bool CalibrationArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
    if (display_width != get_width() ||
           display_height != get_height()) {
        set_display_size(get_width(), get_height());
    }
    cr->save();
    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->rectangle(0, 0, get_width(), get_height());
    cr->fill();

    cr->restore();
    return true;
}

void CalibrationArea::redraw() {
    Glib::RefPtr<Gdk::Window> win = get_window();
    if (win) {
        const Gdk::Rectangle rect(0, 0, display_width, display_height);
        win->invalidate_rect(rect, false);
    }
}

bool CalibrationArea::on_timer_signal() {
    Glib::TimeVal now;
    now.assign_current_time();
    double delta = (now.as_double() - start_time);
    if (delta > time_seconds) {
	app->quit();
    } else
       draw_points();
    return true;
}

bool CalibrationArea::on_button_press_event(GdkEventButton *event) {
    (void) event;
    return true;
}

bool CalibrationArea::on_key_press_event(GdkEventKey *event) {
    (void) event;
    app->quit();
    return true;
}

void swap_double(double *A, double *B) {
    double tmp = *A;
    *A = *B;
    *B = tmp;
}

void CalibrationArea::calib_calc(calib_out_t &out) {
    double coord0, coord1;
    double resX = display_width;
    double resY = display_height;

    if (rotate_points) {
       point_t tmp = points[0];
       points[0] = points[3];
       points[3] = tmp;
       tmp = points[1];
       points[1] = points[2];
       points[2] = tmp;
    }
    out.invert_x = false;
    out.invert_y = false;
    out.swap_xy  = false;

    if (autorotate) {
       if ((abs(points[0].X - points[1].X) < 100.0) && (abs(points[0].X - points[2].X) > 100) && (abs(points[0].Y - points[1].Y) > 100) && (abs(points[0].Y - points[2].Y) < 100)) {
           for (int n = 0; n < 4; n++) {
               swap_double(&points[n].Y, &points[n].X);
           }
           out.swap_xy = true;
       }
       double tmp_pt;
       if (points[0].X > points[1].X && points[2].X > points[3].X) {
           swap_double(&points[0].X, &points[1].X);
           swap_double(&points[2].X, &points[3].X);
           out.invert_x = true;
       }
       if (points[0].Y > points[2].Y && points[1].Y > points[3].Y) {
           swap_double(&points[0].Y, &points[2].Y);
           swap_double(&points[1].Y, &points[3].Y);
           out.invert_y = true;
       }
    }
    // расчёт минимумов тачскрина по оси X
    coord0 = (points[0].X * draws[1].X - points[1].X * draws[0].X) / (draws[1].X - draws[0].X);
    coord1 = (points[2].X * draws[3].X - points[3].X * draws[2].X) / (draws[3].X - draws[2].X);
    out.xmin = int(round((coord0 + coord1) / 2));
    // расчёт максимумов тачскрина по оси X
    coord0 = ((points[0].X * draws[1].X + (resX * points[1].X - resX * points[0].X - points[1].X * draws[0].X)) / (draws[1].X - draws[0].X));
    coord1 = ((points[2].X * draws[3].X + (resX * points[3].X - resX * points[2].X - points[3].X * draws[2].X)) / (draws[3].X - draws[2].X));
    out.xmax = int(round((coord0 + coord1) / 2));
    // расчёт минимумов тачскрина по оси Y
    coord0 = (points[0].Y * draws[2].Y - points[2].Y * draws[0].Y) / (draws[2].Y - draws[0].Y);
    coord1 = (points[1].Y * draws[3].Y - points[3].Y * draws[1].Y) / (draws[3].Y - draws[1].Y);
    out.ymin = int(round((coord0 + coord1) / 2));
    // расчёт максимумов тачскрина по оси Y
    coord0 = ((points[0].Y * draws[2].Y + (resY * points[2].Y - resY * points[0].Y - points[2].Y * draws[0].Y)) / (draws[2].Y - draws[0].Y));
    coord1 = ((points[1].Y * draws[3].Y + (resY * points[3].Y - resY * points[1].Y - points[3].Y * draws[1].Y)) / (draws[3].Y - draws[1].Y));
    out.ymax = int(round((coord0 + coord1) / 2));

    return;
}

static bool get_device_name(Display *dpy, int deviceid, char *buff) {
    Atom                act_type;
    int                 act_format;
    unsigned long       nitems, bytes_after;
    unsigned char       *data;
    bool                result = false;

    buff[0] = 0;
    Atom propdev = XInternAtom(dpy, "Device Node", False);

    if (XIGetProperty(dpy, deviceid, propdev, 0, 1000, False,
                           AnyPropertyType, &act_type, &act_format,
                           &nitems, &bytes_after, &data) == Success && nitems > 0) {
        if (act_type == XA_STRING && act_format == 8) {
                strcpy(buff, (char*)data);
                result = true;
        }
        XFree(data);
    }
    return result;
}

bool search_touchscreen_device(Display *display, int deviceid, char *name) {
    XIDeviceInfo     *info  = NULL;
    XIDeviceInfo     *dev   = NULL;
    XITouchClassInfo *touch = NULL;

    bool result = false;
    int i, j, ndevices;

    info = XIQueryDevice(display, XIAllDevices, &ndevices);

    for (i = 0; i < ndevices; i++) {
        dev = info + i;
        if (dev->deviceid == deviceid) {
             if (get_device_name(display, dev->deviceid, name))
                 result = true;
             break;
        }
    }
    XIFreeDeviceInfo(info);
    return result;
}

void print_device(Display* display)
{
    int ndevices;
    XDeviceInfoPtr list, slist;
    slist = list = (XDeviceInfoPtr) XListInputDevices (display, &ndevices);
    for (int i=0; i<ndevices; i++, list++) {
        if (list->use == IsXKeyboard || list->use == IsXPointer)
            continue;
        XAnyClassPtr any = (XAnyClassPtr) (list->inputclassinfo);
        for (int j=0; j<list->num_classes; j++) {
            if (any->c_class == ValuatorClass) {
                XValuatorInfoPtr V = (XValuatorInfoPtr) any;
                XAxisInfoPtr ax = (XAxisInfoPtr) V->axes;

                if (V->mode == Absolute && V->num_axes >= 2 &&
                       (ax[0].min_value != -1 || ax[0].max_value != -1) &&
                       (ax[1].min_value != -1 || ax[1].max_value != -1)) {
                        printf("Device \"%s\" id=%i\n", list->name, (int)list->id);
                }

            }
            any = (XAnyClassPtr) ((char *) any + any->length);
        }

    }
    XFreeDeviceList(slist);
}

static void usage(char* cmd) {
    fprintf(stderr, "Usage: %s [-h|--help] [-m|--monitor <number>] [-d|--device <device_id>]\n", cmd);
    fprintf(stderr, "\t-h, --help: print this help message\n");
    fprintf(stderr, "\t-m, --monitor: using given monitor number for output\n");
    fprintf(stderr, "\t-l, --list: list calibratable input devices and quit\n");
    fprintf(stderr, "\t-d, --device: using given input device ID for calibration\n");
    fprintf(stderr, "\t-r, --rotate: invert array of calibration points\n");
    fprintf(stderr, "\t-n, --noauto: disable axis inversion auto-detection\n");
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
    int event_base, error_base, deviceid = 0, monitornum = 0;
    int major, minor;
    char buff[64];
    bool list_devices = false;
    bool autorotate = true;

    int flag_daemon = 0;
    const char* short_opts = "hm:d:rln";
    static struct option long_opts[] = {
                    { "help", no_argument, 0, 'h'},
                    { "monitor", required_argument, 0, 'm'},
                    { "rotate", no_argument, 0, 'r'},
                    { "device", required_argument, 0, 'd'},
                    { "list", no_argument, 0, 'l'},
                    { "noauto", no_argument, 0, 'n'},
                    { 0, 0, 0, 0 }
    };
    int option_index = (-1), ch;
    while ((ch = getopt_long(argc, argv, short_opts, long_opts, &option_index)) != -1) {
       switch (ch) {
         case 'm':
             if (optarg)
                monitornum = atoi(optarg); 
             else {
                fprintf(stderr, "Required argument: monitor number.\n");
                usage(argv[0]);
             }
             break;
         case 'd':
             if (optarg)
                deviceid = atoi(optarg); 
             else {
                fprintf(stderr, "Required argument: input device ID.\n");
                usage(argv[0]);
             }
             break;
         case 'l':
             list_devices = true;
             break;
         case 'n':
             autorotate = false;
             break;
         case 'r':
             rotate_points = true;
             break;
         case 'h':
             usage(argv[0]);
             break;
         case '?': default:
             usage(argv[0]);
             break;
       }
       option_index = (-1);
    }
    if (deviceid == 0 && list_devices == false) {
	usage(argv[0]);
    }
    if (list_devices == false && getuid() != 0) {
	fprintf(stderr, "You must be a root!\n");
        return EXIT_FAILURE;
    }
    display = XOpenDisplay(getenv("DISPLAY"));

    if (display == NULL) {
        fprintf(stderr, "Unable to connect to X server\n");
        return EXIT_FAILURE;
    }
    if (list_devices) {
        print_device(display);
        return EXIT_SUCCESS;
    }
    char devicename[64];
    if (search_touchscreen_device(display, deviceid, devicename)) {
        if (!Glib::thread_supported()) Glib::thread_init();
	Gtk::Main app(argc, argv);
	Gtk::Window win;
	Glib::RefPtr<Gdk::Screen> screen = win.get_screen();
//	Glib::RefPtr<Gdk::Display> displayRef = screen->get_display();
	int n_monitors = screen->get_n_monitors();
	if (monitornum >= n_monitors) monitornum = 0;
	Gdk::Rectangle destRect;
	screen->get_monitor_geometry(monitornum, destRect);
	win.move(destRect.get_x(), destRect.get_y());
	win.set_default_size(destRect.get_width(), destRect.get_height());
	win.fullscreen();
	CalibrationArea area(&app, devicename, deviceid, autorotate);
	win.add(area);
	area.set_display_size(win.get_width(), win.get_height());
	area.show();
	app.run(win);
    } else {
        fprintf(stderr, "Evdev device with id %d not found.\n", deviceid);
    }
    XCloseDisplay(display);
    return EXIT_SUCCESS;
}
